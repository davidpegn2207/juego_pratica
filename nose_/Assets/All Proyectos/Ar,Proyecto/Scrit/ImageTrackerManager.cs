using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Video;
using UnityEngine.XR.ARSubsystems;
using System;

public class ImageTrackerManager : MonoBehaviour
{
    [SerializeField] private ARTrackedImageManager aRTrackedImanager;
    private VideoPlayer videoPlayer;
    private bool isImageTrackable;
    // Start is called before the first frame update

    private void OnEnable()
    {
        aRTrackedImanager.trackedImagesChanged += OnImageChanged;
    }
    private void OnDisable()
    {
        aRTrackedImanager.trackedImagesChanged -= OnImageChanged;
    }

    private void OnImageChanged(ARTrackedImagesChangedEventArgs evenData)
    {
        foreach (var teackedImage in evenData.added)
        {
            videoPlayer = teackedImage.GetComponentInChildren<VideoPlayer>();
            videoPlayer.Play();
        }
        foreach (var teackedImage in evenData.updated)
        {
            if(teackedImage.trackingState== TrackingState.Tracking)
            {
              if(!isImageTrackable)
              {
                    isImageTrackable = true;
                    videoPlayer.gameObject.SetActive(true);
                    videoPlayer.Play();
              }
            } 
            else if (teackedImage.trackingState== TrackingState.Limited)
            {
                if (isImageTrackable)
                {
                    isImageTrackable = false;
                    videoPlayer.gameObject.SetActive(false);
                    videoPlayer.Pause();
                }
            }
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
