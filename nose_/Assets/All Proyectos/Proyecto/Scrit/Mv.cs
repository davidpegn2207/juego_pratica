using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mv : MonoBehaviour
{
 public Rigidbody rb;

    //correr y salto
    public float movementSPeed, movementRotate,fuerzaDeSalto;
    public int limiteDeSaltos = 0;
    public bool dejaDecorrer, puedeSaltar = false;

    //camara
    //public Camera camara;
    //public Vector3 offset;
    //public Transform target;
    //[Range(0, 1)] public float lerpValue;
    //public float sensibilidad;

    private void Start()
    {
      //target= GameObject.Find("Player").transform;
    }
    private void LateUpdate()
    {
        //camara.transform.position = Vector3.Lerp(transform.position, target.position + offset, lerpValue);
        //offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X")*sensibilidad,Vector3.up)*offset;
        //transform.LookAt(target);
    }
    void Update()
    {
        Vector3 moventDrection = new Vector3(0, 0, Input.GetAxis("Vertical"));
        transform.Translate((moventDrection * movementSPeed) * Time.deltaTime);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal")*movementRotate, 0));

        if (Input.GetButtonDown("Fire2"))
        {

            StartCoroutine("correr");
            dejaDecorrer = true;
        }
        if (!Input.GetButton("Fire2") && dejaDecorrer == true)
        {
           
            StartCoroutine("Nocorrer");
            dejaDecorrer = false;
        }
        if (Input.GetButton("Jump")&&limiteDeSaltos<=0)
        {
            StartCoroutine("salto");
        }
        if (!Input.GetButton("Jump") && puedeSaltar == true)
        {

            
            puedeSaltar= false;
        }
       
    }
    IEnumerator correr()
    {
        movementSPeed = movementSPeed + 3;
        yield return new WaitForSeconds(2);
        movementSPeed = movementSPeed + 7;
    }
    IEnumerator Nocorrer()
    {
        movementSPeed = movementSPeed - 3;
        yield return new WaitForSeconds(2);
        movementSPeed = movementSPeed - 7;
    }

    IEnumerator salto()
    {
        limiteDeSaltos = 1;
        puedeSaltar = true;

        if (puedeSaltar == true)
        {
            rb.AddForce(new Vector3(0, fuerzaDeSalto, 0), ForceMode.Impulse);
            yield return new WaitForSeconds(2);
            limiteDeSaltos = 0;
        }
    }



}

